//we import the library we will use for controlling our leds
#include <FastLED.h>

//we define constants for the number of strips, the number of leds per strip and the total number of leds
#define NUM_STRIPS 6
#define NUM_LEDS_STRIP 9
#define NUM_LEDS NUM_LEDS_STRIP * NUM_STRIPS
#define RECT_WIDTH 1
#define RECT_HEIGHT 1
#define POSITION_INCR 0.6

//we define an array that will carry all the colour values for our leds
CRGB leds[NUM_LEDS_STRIP * NUM_STRIPS];

//we create a datatype to store rgb color values in
struct RGBColor {
  int r;
  int g;
  int b;
};

//we define two start colors
RGBColor c1 = {0, 255, 0};
RGBColor c2 = {0, 0, 255};
float colorIncr = 0.01;
float colorFrac = 0;

//we define variables for the position
float currX = random(NUM_LEDS_STRIP - RECT_WIDTH);
float currY = random(NUM_STRIPS - RECT_HEIGHT);
float posXIncr = POSITION_INCR;
float posYIncr = POSITION_INCR;

void setup() {
  Serial.begin(9600);

  //we make sure that the library knows where all our strips and leds are on the arduino
  FastLED.addLeds<NEOPIXEL, 6>(leds, 0, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 7>(leds, NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 8>(leds, 2 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 9>(leds, 3 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 10>(leds, 4 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 11>(leds, 5 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
}

void loop() {
  //the main loop
  //we first clear any previous frame in the leds array
  clearLeds();
  
  //now we draw two lines, each with a color that we calculate based on the colorFrac value using the colorLerp function you can see below
  RGBColor cLerp1 = colorLerp(c1, c2, colorFrac);
  RGBColor cLerp2 = colorLerp(c2, c1, colorFrac);

  //we check whether the rectangle has reached the boundaries of the "screen"
  //if it has we make sure that it will move away from the edge
  if(currX <= POSITION_INCR){
    posXIncr = POSITION_INCR;
  }
  if(currX >= NUM_LEDS_STRIP - RECT_WIDTH - POSITION_INCR){
    posXIncr = -POSITION_INCR;
  }
  if(currY <= POSITION_INCR){
    posYIncr = POSITION_INCR;
  }
  if(currY >= NUM_STRIPS - RECT_HEIGHT - POSITION_INCR){
    posYIncr = -POSITION_INCR;
  }

  //we draw our rectangle with the updated postions
  drawRect(floor(currX), floor(currY), RECT_WIDTH, RECT_HEIGHT, cLerp1);
  
  FastLED.show();

  //we add an increment to the colorFrac value and when it goes outside the range 0-1 we make the increment either positive or negative
  //this is what creates the smooth fading from green-blue-green-etc-etc
  colorFrac += colorIncr;
  if(colorFrac > 1 || colorFrac < 0){
    colorIncr *= -1;  
  }

  //here we update the position of the rectangle
  currX += posXIncr;
  currY += posYIncr;

  //the delay makes sure the current "frame" is visible for a specified amount of time
  delay(100);
}


//the drawRect function simply uses 4 x a drawLine function to draw a rectangle
void drawRect(int x, int y, int width, int height, RGBColor color){
  drawLine(x, y, x, y + height, color);
  drawLine(x, y + height, x + width, y + height, color);
  drawLine(x + width, y + height, x + width, y, color);
  drawLine(x + width, y, x, y, color);
}

//a function for drawing a line between two points
void drawLine(int x1, int y1, int x2, int y2, RGBColor color){
  int dx = x2 - x1;
  int dy = y2 - y1;
  int steps = 0;
  if(abs(dx) > abs(dy)){
    steps = abs(dx);  
  } else {
    steps = abs(dy);
  }
  
  float xIncr = dx / (float)steps;
  float yIncr = dy / (float)steps;

  float x = x1;
  float y = y1;

  setLed(x1, y1, color);
  for(int i = 0; i < steps; i++){
    x += xIncr;
    y += yIncr;
    setLed(round(x), round(y), color);
  }
}

//the colorLerp function we can use to calculate a color between two colors given a number between 0-1
RGBColor colorLerp(RGBColor start, RGBColor end_, float frac){
  RGBColor c = start;
  float dr = end_.r - start.r;
  float dg = end_.g - start.g;
  float db = end_.b - start.b;

  if(frac > 1.0) frac = 1.0;
  if(frac < 0.0) frac = 0.0;

  c.r += floor(dr * frac);
  c.g += floor(dg * frac);
  c.b += floor(db * frac);

  return c;
}

//setLed is a function to set the color of the led by coordinate
void setLed(int x, int y, RGBColor color){
  //we calculate the index of the led in the array leds
  //depending on the orientation of your strips you can swap these
  //int index = (x * NUM_LEDS_STRIP) + y;
  int index = (y * NUM_LEDS_STRIP) + x;
  leds[index].setRGB(color.r, color.g, color.b);
}

//we use clearLeds to clear the screen and put all leds to black, this will enable us to make an animation
void clearLeds(){
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i].setRGB(0, 0, 0);  
  }
}
