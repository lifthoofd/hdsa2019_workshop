#include <CapacitiveSensor.h>

#define SMOOTH_ALPHA 0.4

float smoothVal = 0;
CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired

void setup()                    
{
   cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
   Serial.begin(9600);
}

void loop()                    
{
    long start = millis();
    long val =  cs_4_2.capacitiveSensor(30);

    Serial.print(millis() - start);        // check on performance in milliseconds
    Serial.print("\t");                    // tab character for debug windown spacing

    Serial.print(val);                  // print sensor output 1
    Serial.print("\t");
    Serial.println(smooth(val));            // print smoothed value

    delay(10);                             // arbitrary delay to limit data to serial port 
}

// returns the value smoothed with an exponential filter
float smooth(long rawVal)
{
  smoothVal = ((float)rawVal * SMOOTH_ALPHA) + (smoothVal * (1 - SMOOTH_ALPHA));
  return smoothVal;
}
