//we import the library we will use for controlling our leds
#include <FastLED.h>

//we define constants for the number of strips, the number of leds per strip and the total number of leds
#define NUM_STRIPS 6
#define NUM_LEDS_STRIP 9
#define NUM_LEDS NUM_LEDS_STRIP * NUM_STRIPS
#define CHANCE 0.9

//we define an array that will carry all the colour values for our leds
CRGB leds[NUM_LEDS_STRIP * NUM_STRIPS];

void setup() {
//  Serial.begin(9600);

  //we make sure that the library knows where all our strips and leds are on the arduino
  FastLED.addLeds<NEOPIXEL, 6>(leds, 0, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 7>(leds, NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 8>(leds, 2 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 9>(leds, 3 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 10>(leds, 4 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 11>(leds, 5 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
}

void loop() {
  //the main loop, here we cycle through all our leds
  for(int i = 0; i < NUM_LEDS; i++){
    //to dermine whether we wanna set a colour or make our led black we use chance
    //we get a random number between 0 and 1 and the we see if that number is higher then a predetermined chance
    if((float)random(100) / 100. > CHANCE){
      int randR = floor(random(256));
      int randB = floor(random(256));
      leds[i].setRGB(randR, 0, randB);  
    } else {
      leds[i].setRGB(0, 0, 0);  
    }
  }
  //after we have updated all our leds we use FastLED.show() to send it to our strips connected to the arduino
  FastLED.show();
  //the delay makes sure the current "frame" is visible for a specified amount of time
  delay(75);
}
