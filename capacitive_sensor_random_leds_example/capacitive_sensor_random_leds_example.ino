//we import the library we will use for controlling our leds
#include <FastLED.h>
// we import the library we will use for the capacitive sensing
#include <CapacitiveSensor.h>

//we define some global constants
#define NUM_STRIPS 6
#define NUM_LEDS_STRIP 9
#define NUM_LEDS NUM_LEDS_STRIP * NUM_STRIPS
#define SMOOTH_ALPHA 0.6
#define LED_LOOP_RATE 75
#define CAP_SENS_THRESH 60

//we define an array that will carry all the colour values for our leds
CRGB leds[NUM_LEDS_STRIP * NUM_STRIPS];
long ledMillis = 0;
float chance = 0;

float smoothVal = 0;
CapacitiveSensor   cs_4_2 = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired

void setup() {
  Serial.begin(9600);

  //we make sure that the library knows where all our strips and leds are on the arduino
  FastLED.addLeds<NEOPIXEL, 6>(leds, 0, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 7>(leds, NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 8>(leds, 2 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 9>(leds, 3 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 10>(leds, 4 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 11>(leds, 5 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
}

void loop() {

  float val =  smooth(cs_4_2.capacitiveSensor(30));           // here we get our capacitive sensor value and immidiately smooth it, too take out any potential spikes

//  Serial.println(val);
  //instead of using a fixed chance, now we use two chances, a low and a high one. When the capacitive value goes above a certain value there will more leds lighting up
  if(val > CAP_SENS_THRESH){
    chance = 0.5;
  } else {
    chance = 0.95;
  }

  // here we seperate the loop for the leds from the main loop so we can run both at a different speed
  if(millis() - ledMillis > LED_LOOP_RATE){
    ledsLoop();
    ledMillis = millis();
  }
}


void ledsLoop(){
  //the main loop for the leds, here we cycle through all our leds
  for(int i = 0; i < NUM_LEDS; i++){
    //to dermine whether we wanna set a colour or make our led black we use chance
    //we get a random number between 0 and 1 and the we see if that number is higher then a predetermined chance
    if((float)random(100) / 100. > chance){
      int randR = floor(random(256));
      int randB = floor(random(256));
      leds[i].setRGB(randR, 0, randB);  
    } else {
      leds[i].setRGB(0, 0, 0);  
    }
  }
  //after we have updated all our leds we use FastLED.show() to send it to our strips connected to the arduino
  FastLED.show();
}

// returns the value smoothed with an exponential filter
float smooth(long rawVal)
{
  smoothVal = ((float)rawVal * SMOOTH_ALPHA) + (smoothVal * (1 - SMOOTH_ALPHA));
  return smoothVal;
}
