# Install arduino IDE and libraries

* Install the arduino IDE from here:[https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)

* Install the libraries we need via the **Library Manager**. 
    * You can read how to do that here: <https://www.arduino.cc/en/guide/libraries#toc3>.
    * search for the following libraries in the **Library Manager**: 
        * FastLED
        * CapacitiveSensor

# Wiring up electronics
* The connection diagram for the leds: (**DO NOT CUT UP THE LED STRIP UNTIL YOU KNOW HOW YOU WANT TO USE THEM IN YOUR WORK**)
![leds diagram](https://gitlab.com/lifthoofd/hdsa2019_workshop/raw/3c189a4d214874d0ac56ac03a2b86e3db3cc96c7/example_wiring.png)

* The connection diagram for the capacitive sensor, the value for the resistor is 1M:
![cap sense diagram](https://gitlab.com/lifthoofd/hdsa2019_workshop/raw/3c189a4d214874d0ac56ac03a2b86e3db3cc96c7/example_cap_sens_wiring.png)

# Examples
## Led examples
1. random_example
2. rainbow_example
3. line_example
4. line_color_animation_example
5. rectangle_example
6. rectangle_position_animation_example

## Capacitive sensor examples
1. capacitive_sensor_example
2. capacitive_sensor_random_leds_example
