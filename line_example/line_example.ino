//we import the library we will use for controlling our leds
#include <FastLED.h>

//we define constants for the number of strips, the number of leds per strip and the total number of leds
#define NUM_STRIPS 6
#define NUM_LEDS_STRIP 9
#define NUM_LEDS NUM_LEDS_STRIP * NUM_STRIPS

//we define an array that will carry all the colour values for our leds
CRGB leds[NUM_LEDS_STRIP * NUM_STRIPS];

//we create a datatype to store rgb color values in
struct RGBColor {
  int r;
  int g;
  int b;
};

void setup() {
  Serial.begin(9600);

  //we make sure that the library knows where all our strips and leds are on the arduino
  FastLED.addLeds<NEOPIXEL, 6>(leds, 0, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 7>(leds, NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 8>(leds, 2 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 9>(leds, 3 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 10>(leds, 4 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
  FastLED.addLeds<NEOPIXEL, 11>(leds, 5 * NUM_LEDS_STRIP, NUM_LEDS_STRIP);
}

void loop() {
  //the main loop
  //we first clear any previous frame in the leds array
  clearLeds();
  
  //now we draw two lines, each with a specific color
  RGBColor c1 = {0, 255, 0};
  RGBColor c2 = {0, 0, 255};
  drawLine(0, 0, 8, 5, c1);
  drawLine(0, 5, 8, 0, c2);
  
  FastLED.show();
  //the delay makes sure the current "frame" is visible for a specified amount of time
  delay(100);
}

//a function for drawing a line between two points
void drawLine(int x1, int y1, int x2, int y2, RGBColor color){
  int dx = x2 - x1;
  int dy = y2 - y1;
  int steps = 0;
  if(abs(dx) > abs(dy)){
    steps = abs(dx);  
  } else {
    steps = abs(dy);
  }
  
  float xIncr = dx / (float)steps;
  float yIncr = dy / (float)steps;

  float x = x1;
  float y = y1;

  setLed(x1, y1, color);
  for(int i = 0; i < steps; i++){
    x += xIncr;
    y += yIncr;
    setLed(round(x), round(y), color);
  }
}

//setLed is a function to set the color of the led by coordinate
void setLed(int x, int y, RGBColor color){
  //we calculate the index of the led in the array leds
  //depending on the orientation of your strips you can swap these
  //int index = (x * NUM_LEDS_STRIP) + y;
  int index = (y * NUM_LEDS_STRIP) + x;
  leds[index].setRGB(color.r, color.g, color.b);
}

//we use clearLeds to clear the screen and put all leds to black, this will enable us to make an animation
void clearLeds(){
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i].setRGB(0, 0, 0);  
  }
}
